package io.techniktom.currencycalculator.api.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import org.jetbrains.annotations.NotNull;

import io.techniktom.currencycalculator.api.tools.DoubleDeserializer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Currency {

    @JsonProperty(value = "kod")
    @ColumnInfo(name = "code")
    @PrimaryKey
    @NotNull
    private String code;

    @JsonProperty(value = "mena")
    @ColumnInfo(name = "currency")
    private String currency;

    @JsonProperty(value = "mnozstvi")
    @ColumnInfo(name = "amount")
    private Integer amount;

    @JsonDeserialize(using = DoubleDeserializer.class)
    @JsonProperty(value = "kurz")
    @ColumnInfo(name = "rate")
    private double rate;

    @JsonProperty(value = "zeme")
    @ColumnInfo(name = "country")
    private String country;
}
