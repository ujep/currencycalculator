package io.techniktom.currencycalculator.api.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Currencies {
    @JsonProperty(value = "tabulka")
    private Table table;

    @JsonProperty(value = "banka")
    private String bank;

    @JsonProperty(value = "datum")
    private String date;

    @JsonProperty(value = "poradi")
    private int order;
}
