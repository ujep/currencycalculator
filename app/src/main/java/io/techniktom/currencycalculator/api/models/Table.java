package io.techniktom.currencycalculator.api.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Table {
    @JacksonXmlElementWrapper(useWrapping = false)
    @JsonProperty(value = "radek")
    private List<Currency> currencies;

    @JsonProperty(value = "typ")
    private String type;
}
