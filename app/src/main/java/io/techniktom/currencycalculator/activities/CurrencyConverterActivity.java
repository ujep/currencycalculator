package io.techniktom.currencycalculator.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import org.apache.commons.lang3.math.NumberUtils;

import io.techniktom.currencycalculator.R;

public class CurrencyConverterActivity extends AppCompatActivity {
    private static final int floatingSpaces = 3;
    private boolean internalRecalculation = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.currency_convertor);

        Intent intent = getIntent();
        String code = intent.getStringExtra("code");
        Double rate = intent.getDoubleExtra("rate", 1);

        TextView foreignCode = findViewById(R.id.foreignCode);
        foreignCode.setText(code);

        Button clearButton = findViewById(R.id.clear_btn);

        EditText localNumber = findViewById(R.id.localNumber);
        EditText foreignNumber = findViewById(R.id.foreignNumber);

        clearButton.setOnClickListener(v -> {
            internalRecalculation = true;
            foreignNumber.setText("");
            internalRecalculation = true;
            localNumber.setText("");
        });

        localNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if(internalRecalculation) {
                    internalRecalculation = false;
                    return;
                }

                internalRecalculation = true;
                foreignNumber.setText(calculateAndFormat(localNumber, rate, false));
            }
        });

        foreignNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if(internalRecalculation) {
                    internalRecalculation = false;
                    return;
                }

                internalRecalculation = true;
                localNumber.setText(calculateAndFormat(foreignNumber, rate, true));
            }
        });
    }

    private String calculateAndFormat(EditText number, Double rate, boolean mul) {
        String sNumber = number.getText().toString();

        if(NumberUtils.isCreatable(sNumber)) {
            double value = Double.parseDouble(sNumber);

            if(mul)
                value *= rate;
            else
                value /= rate;

            return String.valueOf(Math.round(value*Math.pow(10, floatingSpaces)) / Math.pow(10, floatingSpaces));
        }

        return "NaN";
    }
}
