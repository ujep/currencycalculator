package io.techniktom.currencycalculator;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.widget.TableLayout;
import android.widget.Toast;

import io.techniktom.currencycalculator.db.repository.Repositories;
import io.techniktom.currencycalculator.services.DataRefreshService;
import io.techniktom.currencycalculator.tools.Intents;
import io.techniktom.currencycalculator.tools.MainWindowRefreshBroadcastReceiver;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        Repositories.init(getApplicationContext());
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        TableLayout tl = findViewById(R.id.currencies_table);
        tl.setColumnStretchable(0, true);
        tl.setColumnShrinkable(1, true);
        tl.setColumnShrinkable(2, true);
        tl.setColumnShrinkable(3, true);


        DataRefreshService.enqueueWork(getApplicationContext(), new Intent(Intents.LOAD_CURRENCIES));

        SwipeRefreshLayout sv = findViewById(R.id.container);
        sv.setOnRefreshListener(() -> DataRefreshService.enqueueWork(getApplicationContext(), getIntent()));

        registerReceiver(
                new MainWindowRefreshBroadcastReceiver(tl, sv),
                new IntentFilter(Intents.RELOAD_CURRENCIES)
        );

        Toast.makeText(this, "Data is loading", Toast.LENGTH_LONG).show();
    }
}