package io.techniktom.currencycalculator.tools;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.function.BiConsumer;

import io.techniktom.currencycalculator.R;
import io.techniktom.currencycalculator.api.models.Currency;

public class Tools {
    public static View createViewFromLayout(Context context, int layoutId) {
       return LayoutInflater.from(context).inflate(layoutId, null);
    }

    public static TableRow createTableRow(Context context, Currency currency, BiConsumer<Currency, View> clickCallBack) {
        View root = createViewFromLayout(context, R.layout.currency_row);
        TextView nameCountryText = root.findViewById(R.id.name_country);
        TextView convertInfoText = root.findViewById(R.id.convert_info);

        nameCountryText.setText(String.format("%s (%s)", formatName(currency.getCurrency()), currency.getCountry()));
        convertInfoText.setText(String.format("%s %s = %s CZK", currency.getAmount(), currency.getCode(), currency.getRate()));

        root.setOnClickListener((v) -> clickCallBack.accept(currency, v));

        return (TableRow) root;
    }

    private static String formatName(String name) {
        char[] chars = name.toCharArray();

        chars[0] = name.toUpperCase().charAt(0);

        return new String(chars);
    }
}
