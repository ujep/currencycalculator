package io.techniktom.currencycalculator.db.repository;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;
import java.util.Optional;

import io.techniktom.currencycalculator.api.models.Currency;

@Dao
public abstract class CurrencyRepository {
    @Query("SELECT * FROM Currency")
    public abstract List<Currency> getAll();

    @Query("SELECT count(*) > 0 FROM Currency where code = :code")
    public abstract boolean existsByCode(String code);

    @Query("SELECT count(*) FROM Currency")
    public abstract int count();

    @Insert
    public abstract void saveAll(Currency... users);

    @Delete
    public abstract void delete(Currency user);

    @Query("SELECT * FROM Currency WHERE code = :code LIMIT 1")
    public abstract Optional<Currency> getByCode(String code);

    @Update
    public abstract void update(Currency currency);

    @Insert
    public abstract void insert(Currency currency);
}
