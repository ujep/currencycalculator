package io.techniktom.currencycalculator.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import io.techniktom.currencycalculator.api.models.Currency;
import io.techniktom.currencycalculator.db.repository.CurrencyRepository;

@Database(entities = {Currency.class}, version = 1)
public abstract class CurrencyDB extends RoomDatabase {
    public abstract CurrencyRepository currencyRepository();
}
