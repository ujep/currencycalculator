package io.techniktom.currencycalculator.db.repository;

import android.content.Context;

import androidx.room.Room;

import io.techniktom.currencycalculator.db.CurrencyDB;

public class Repositories {
    private static CurrencyRepository currencyRepository;

    public static void init(Context context) {
        String databaseCurrencyFilename = "currencies.db";

        currencyRepository = Room
                .databaseBuilder(context, CurrencyDB.class, databaseCurrencyFilename)
                .build()
                .currencyRepository();
    }

    public static CurrencyRepository getCurrencyRepository() {
        return currencyRepository;
    }
}
